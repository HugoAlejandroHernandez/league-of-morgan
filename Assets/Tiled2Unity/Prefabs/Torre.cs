﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : MonoBehaviour
{

    private GameObject enemigo;
    private bool esta_activa;
    public GameObject bala;
    public float Hurt = 10f;
    private float distancia_umbral = 10;

    public GameObject Enemigo
    {
        get
        {
            return enemigo;
        }

        set
        {
            enemigo = value;
        }
    }

    public bool Esta_activa
    {
        get
        {
            return esta_activa;
        }

        set
        {
            esta_activa = value;
        }

    }




    GameObject BuscarEnemigoCercano()
    {
        ArrayList enemigos = PoolingUnidades.unidades;
        GameObject temp;
        foreach (Object item in enemigos)
        {
            temp = (GameObject)item;
            if (Vector3.Distance(temp.transform.position, this.transform.position) < distancia_umbral)
            {
                return temp;
            }
        }
        return null;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Enemigo = BuscarEnemigoCercano();
        if (Enemigo != null)
        {
            Debug.DrawLine(this.transform.position, enemigo.transform.position, Color.yellow);
        }
        float dist = (Enemigo.transform.position - transform.position).magnitude;
        /* Enemigo = BuscarEnemigoCercano();
         if (Enemigo != null)
         {
             Debug.DrawLine(this.transform.position, enemigo.transform.position, Color.yellow);
         }
         float dist = (Enemigo.transform.position - transform.position).magnitude;

         if (dist <= distancia_umbral)
         {
             Debug.DrawLine(this.transform.position, Enemigo.transform.position, Color.red);
             Disparar();
         }*/
    }
    void Disparar()
    {
        GameObject obj = (GameObject)Instantiate(GameObject.Find("bala"), this.transform.position, Quaternion.identity);
        Bala bala = obj.GetComponent<Bala>();
        //bala.ActivarBala(this);
    }

    /* GameObject BuscarEnemigoCercano()
     {
         ArrayList enemigos = PoolingUnidades.unidades;
         foreach (GameObject item in enemigos)
         {

             if (Vector3.Distance(item.transform.position, this.transform.position) < distancia_umbral) ;
             {
             return item;
             }
     }
     return null;

}*/

}

    

