﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaBarraVida : Unidad {


    public GameObject unidad0_1;
    public GameObject barra_verde;
    public GameObject barra_roja;
    float lifeInicio;
    float lifeActual;
    float escala = .001f;
    float escala01 = 0f;
    SpriteRenderer sr;

	// Use this for initialization
	void Start () {
        sr = barra_verde.GetComponent<SpriteRenderer>();
        lifeInicio = unidad0_1.GetComponent<Unidad>().Vida;
        lifeActual = lifeInicio;

	}
	
	// Update is called once per frame
	void Update () {
        escala = (lifeInicio - lifeActual) / 100f;
        if (sr.transform.localScale.x > 0)
        {
            sr.transform.localScale -= new Vector3(escala, 0);
            escala01 -= 1.85f * escala;
            

        }
        
        barra_verde.transform.position = unidad0_1.transform.position - new Vector3(1.1f - sr.bounds.size.x / 2, -0.8f);
        barra_roja.transform.position = unidad0_1.transform.position - new Vector3(-0.082f, -0.469999f);

        lifeInicio = lifeActual;
        lifeActual = unidad0_1.GetComponent<Unidad>().Vida;
    }
}
