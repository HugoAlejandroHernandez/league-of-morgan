﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unidad : MonoBehaviour
{

    public GameObject ruta;

    private int indice;
    private Vector3 posicion_inicial;
    private Vector3 posicion_siguiente;
    public float Vida = 100f;
    private float vel = 1f;
    private float distancia_punto = 0.5f;
    private bool esta_viva;



    // Use this for initialization
    void Start()
    {
        //posicion_inicial = this.transform.position;
        esta_viva = true;
        posicion_inicial = this.transform.position;
        posicion_siguiente = ruta.transform.GetChild(0).position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir;

        if (esta_viva == true)
        {
            dir = posicion_siguiente - this.transform.position;
            dir.z = 0;
            this.transform.position += dir * vel * Time.deltaTime;

            if (dir.magnitude <= distancia_punto)
            {
                if (indice + 1 < ruta.transform.childCount)
                {
                    indice++;
                    posicion_siguiente = ruta.transform.GetChild(indice).position;
                    //Debug.Log(" xs" + posicion_siguiente.x + " ys" + posicion_siguiente.y);
                }
                else
                {
                    indice = 0;
                    this.transform.position = posicion_inicial;
                    posicion_siguiente = ruta.transform.GetChild(0).position;
                }
            }
        }
    }



    private void OnTriggerEnter2D(Collider2D otro)
    {
        if (otro.gameObject.tag == "bala")
        {
            Destroy(otro.gameObject);
            this.Vida -= otro.gameObject.GetComponent<Bala>().Hurt;
        }
    }

    public bool Esta_viva
    {
        get
        {
            return esta_viva;
        }

        set
        {
            esta_viva = value;
        }
    }
}
